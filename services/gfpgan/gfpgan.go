package gfpgan

import (
	"context"
	"path/filepath"
	"strconv"
)

type Version string

const (
	VersionV12 Version = "1.2"
	VersionV13 Version = "1.3"
	VersionV14 Version = "1.4"
)

type Exec struct {
	py         Python
	gfpganPath string
}

type Python interface {
	ExecCtx(ctx context.Context, codeDir, pyFile string, args ...string) error
}

func New(p Python, gfpganPath string) *Exec {
	abs, _ := filepath.Abs(gfpganPath)
	return &Exec{
		py:         p,
		gfpganPath: abs,
	}
}

// InferenceGfpgan 图像人脸高清修复
// input 输入图像或文件夹
// output 导出目录
// Version GFPGAN 模型版本; 1 | 1.2 | 1.3 | 1.4
func (e *Exec) InferenceGfpgan(ctx context.Context, input, output string, ver Version, options ...[]string) error {
	const py = "inference_gfpgan"
	var args []string
	for _, option := range options {
		args = append(args, option...)
	}
	args = append(args, optionInput(input)...)
	args = append(args, optionOutput(output)...)
	args = append(args, optionVersion(string(ver))...)
	return e.py.ExecCtx(ctx, e.gfpganPath, py, args...)
}

func optionInput(val string) []string {
	return []string{"--input", val}
}

func optionOutput(val string) []string {
	return []string{"--output", val}
}

func optionVersion(val string) []string {
	return []string{"--version", val}
}

// OptionUpScale 图像的最终上采样比例。 默认值：2
func OptionUpScale(val uint) []string {
	return []string{"--upscale", strconv.Itoa(int(val))}
}

// OptionBgUpSampler 背景上采样器。 默认值：realesrgan
// val取值： realesrgan; none
func OptionBgUpSampler(val string) []string {
	return []string{"--bg_upsampler", val}
}

// OptionOnlyCenterFace 只保存中间脸
func OptionOnlyCenterFace() []string {
	return []string{"--only_center_face"}
}

// OptionBgTile 背景采样器的平铺大小，0 表示测试期间没有平铺。 默认值：400
func OptionBgTile(val uint) []string {
	return []string{"--bg_tile", strconv.Itoa(int(val))}
}
